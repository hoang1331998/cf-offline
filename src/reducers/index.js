import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import offlineTeam from './offlineTeam';

export default combineReducers({
  
    offlineTeam,
    form: formReducer,

});
