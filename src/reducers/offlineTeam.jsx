import {GET_OFFLINE_TEAM_LIST,
    GET_OFFLINE_TEAM_LOCATION,
    POST_OFFLINE_TEAM_CREATE}
from '../actions/offlineTeamAction';

var initialState = {
    getOfflineTeamList: false,
    errorOfflineTeamList: false,

    getOfflineTeamLocation: false,
    errorOfflineTeamLocation: false,

    getResponseDataOfflineTeam: false,
    errorResponseDataOfflineTeam: false,
};

const offlineTeam = (state = initialState, action ) =>{

    switch (action.type){
        case GET_OFFLINE_TEAM_LIST:
            return{
                ...state,
                getOfflineTeamList: action.payload.data,
                errorOfflineTeamList: action.payload.errorMessage
            }
        case GET_OFFLINE_TEAM_LOCATION:
            return{
                ...state,
                getOfflineTeamLocation: action.payload.data,
                errorOfflineTeamLocation: action.payload.errorMessage
            }
        case POST_OFFLINE_TEAM_CREATE:
            return{
                ...state,
                getResponseDataOfflineTeam: action.payload.data,
                errorResponseDataOfflineTeam: action.payload.errorMessage
            }
        default:
            return state;   
    }
}
export default offlineTeam;