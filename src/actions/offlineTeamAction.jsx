import axios from 'axios'

export const GET_OFFLINE_TEAM_LIST = "GET_OFFLINE_TEAM_LIST";
export const GET_OFFLINE_TEAM_LOCATION = "GET_OFFLINE_TEAM_LOCATION";
export const POST_OFFLINE_TEAM_CREATE = "POST_OFFLINE_TEAM_CREATE";


const url = "http://dotkich.goplay.vn/api/ajaxapi/OfflineTeamMember/";

export const getOfflineTeamList = () => {
    
    return dispatch => {
        
        axios.post(url + "GetAllTeamRegister")
        .then(function(res){
            let results = res.data.data;
            console.log(results);        
            dispatch({
                type: GET_OFFLINE_TEAM_LIST,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: GET_OFFLINE_TEAM_LIST,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};


export const getOfflineTeamLocation = (id) => {
  
    return dispatch => {
        axios.post(url + "GetTeamLocation/?locationid="+ id)
        .then(function(res){
            let results = res.data.data;
            console.log(results);
            dispatch({
                type: GET_OFFLINE_TEAM_LOCATION,
                payload: {
                    data: results,
                    errorMessage: false
                }
            })
        })
        .catch(function(err){
            dispatch({
                type: GET_OFFLINE_TEAM_LOCATION,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};

export const postOfflineTeamCreate = (data) => {
    
    return dispatch => {
        axios.post(url + "OfflineTeamRegister",data)
        .then(function(res){
            let results = res.message;
            dispatch({
                type: POST_OFFLINE_TEAM_CREATE,
                payload: {
                    data: results,
                    errorMessage: false,                 
                }              
            })
        })
        .catch(function(err){
            dispatch({
                type: POST_OFFLINE_TEAM_CREATE,
                payload:{
                    data: false,
                    errorMessage: err.message
                }
            })
        })
    }
};


/////////Delete cache data---------------
export const deleteDataOfflineTeam = () =>{

    return (dispatch) => {
        
        dispatch({
            type: GET_OFFLINE_TEAM_LOCATION,
            payload: {
                data: false,
                errorMessage: false
            }
        })
        dispatch({
            type: POST_OFFLINE_TEAM_CREATE,
            payload: {
                data: false,
                errorMessage: false
            }
        })
    }
}