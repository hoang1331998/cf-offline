export const RegisterValidations = (values, file) => {
    const errors = {};

    if (!values.LocationId || values.LocationId === "") {
        errors.LocationId = "Vui lòng chọn thành phố của bạn !!!";
    }
    if (!values.TeamName || values.TeamName === "") {
        errors.TeamName = "Tên đội không được bỏ trống !!!";
    }
    if (!values.HeadName || values.HeadName === "") {
        errors.HeadName = "Tên đội trưởng không được bỏ trống !!!";
    }
    if (!values.emailCap || values.emailCap === "") {
        errors.emailCap = "Email không được bỏ trống!!!";
    }
    if (
        values.emailCap &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.emailCap)
    ) {
        errors.emailCap = 'Vui lòng nhập đúng định dạng email'
    }
    if (!values.sdtCap || values.sdtCap === "") {
        errors.sdtCap = "Số điện thoại không được bỏ trống!!!";
    }
    if (
        values.sdtCap &&
        !/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/.test(values.sdtCap)
    ) {
        errors.sdtCap = 'Vui lòng nhập đúng định dạng SĐT'
    }
    if (!values.usernameCap || values.usernameCap === "") {
        errors.usernameCap = "Tên đăng nhập không được bỏ trống!!!";
    }
    if (
        values.usernameCap &&
        !/^[A-Za-z0-9_]{4,21}$/.test(values.usernameCap)
    ) {
        errors.usernameCap = 'Tên đăng nhập phải có độ dài từ 4-21 ký tự'
    }
    if (!file.cmndCap || file.cmndCap === "") {
        errors.cmndCap = "Vui lòng tải ảnh CMND !!!";
    }
    // Thanh vien 1
    if (!values.nameTv1 || values.nameTv1 === "") {
        errors.nameTv1 = "Tên không được bỏ trống!!!";
    }
    if (!values.emailTv1 || values.emailTv1 === "") {
        errors.emailTv1 = "Email không được bỏ trống!!!";
    }
    if (
        values.emailTv1 &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.emailTv1)
    ) {
        errors.emailTv1 = 'Vui lòng nhập đúng định dạng email'
    }
    if (!values.sdtTv1 || values.sdtTv1 === "") {
        errors.sdtTv1 = "Số điên thoại không được bỏ trống!!!";
    }
    if (
        values.sdtTv1 &&
        !/^\d{10}$/.test(values.sdtTv1)
    ) {
        errors.sdtTv1 = 'Vui lòng nhập đúng định dạng SĐT'
    }
    if (!values.usernameTv1 || values.usernameTv1 === "") {
        errors.usernameTv1 = "Tên đăng nhập không được bỏ trống!!!";
    }
    if (
        values.usernameTv1 &&
        !/^[A-Za-z0-9_]{4,21}$/.test(values.usernameTv1)
    ) {
        errors.usernameTv1 = 'Tên đăng nhập phải có độ dài từ 4-21 ký tự'
    }
    // Thanh vien 2
    if (!values.nameTv2 || values.nameTv2 === "") {
        errors.nameTv2 = "Tên không được bỏ trống!!!";
    }
    if (!values.emailTv2 || values.emailTv2 === "") {
        errors.emailTv2 = "Email không được bỏ trống!!!";
    }
    if (
        values.emailTv2 &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.emailTv2)
    ) {
        errors.emailTv2 = 'Vui lòng nhập đúng định dạng email'
    }
    if (!values.sdtTv2 || values.sdtTv2 === "") {
        errors.sdtTv2 = "Số điên thoại không được bỏ trống!!!";
    }
    if (
        values.sdtTv2 &&
        !/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/.test(values.sdtTv2)
    ) {
        errors.sdtTv2 = 'Vui lòng nhập đúng định dạng SĐT'
    }
    if (!values.usernameTv2 || values.usernameTv2 === "") {
        errors.usernameTv2 = "Tên đăng nhập không được bỏ trống!!!";
    }
    if (
        values.usernameTv2 &&
        !/^[A-Za-z0-9_]{4,21}$/.test(values.usernameTv2)
    ) {
        errors.usernameTv2 = 'Tên đăng nhập phải có độ dài từ 4-21 ký tự'
    }
    // Thanh vien 3
    if (!values.nameTv3 || values.nameTv3 === "") {
        errors.nameTv3 = "Tên không được bỏ trống!!!";
    }
    if (!values.emailTv3 || values.emailTv3 === "") {
        errors.emailTv3 = "Email không được bỏ trống!!!";
    }
    if (
        values.emailTv3 &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.emailTv3)
    ) {
        errors.emailTv3 = 'Vui lòng nhập đúng định dạng email'
    }
    if (!values.sdtTv3 || values.sdtTv3 === "") {
        errors.sdtTv3 = "Số điên thoại không được bỏ trống!!!";
    }
    if (
        values.sdtTv3 &&
        !/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/.test(values.sdtTv3)
    ) {
        errors.sdtTv3 = 'Vui lòng nhập đúng định dạng SĐT'
    }
    if (!values.usernameTv3 || values.usernameTv3 === "") {
        errors.usernameTv3 = "Tên đăng nhập không được bỏ trống!!!";
    }
    if (
        values.usernameTv3 &&
        !/^[A-Za-z0-9_]{4,21}$/.test(values.usernameTv3)
    ) {
        errors.usernameTv3 = 'Tên đăng nhập phải có độ dài từ 4-21 ký tự'
    }
    // Thanh vien 4
    if (!values.nameTv4 || values.nameTv4 === "") {
        errors.nameTv4 = "Tên không được bỏ trống!!!";
    }
    if (!values.emailTv4 || values.emailTv4 === "") {
        errors.emailTv4 = "Email không được bỏ trống!!!";
    }
    if (
        values.emailTv4 &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.emailTv4)
    ) {
        errors.emailTv4 = 'Vui lòng nhập đúng định dạng email'
    }
    if (!values.sdtTv4 || values.sdtTv4 === "") {
        errors.sdtTv4 = "Số điên thoại không được bỏ trống!!!";
    }
    if (
        values.sdtTv4 &&
        !/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/.test(values.sdtTv4)
    ) {
        errors.sdtTv4 = 'Vui lòng nhập đúng định dạng SĐT'
    }
    if (!values.usernameTv4 || values.usernameTv4 === "") {
        errors.usernameTv4 = "Tên đăng nhập không được bỏ trống!!!";
    }
    if (
        values.usernameTv4 &&
        !/^[A-Za-z0-9_]{4,21}$/.test(values.usernameTv4)
    ) {
        errors.usernameTv4 = 'Tên đăng nhập phải có độ dài từ 4-21 ký tự'
    }
    // Thanh vien 5
    if (!values.nameTv5 || values.nameTv5 === "") {
        errors.nameTv5 = "Tên không được bỏ trống!!!";
    }
    if (!values.emailTv5 || values.emailTv5 === "") {
        errors.emailTv5 = "Email không được bỏ trống!!!";
    }
    if (
        values.emailTv5 &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.emailTv5)
    ) {
        errors.emailTv5 = 'Vui lòng nhập đúng định dạng email'
    }
    if (!values.sdtTv5 || values.sdtTv5 === "") {
        errors.sdtTv5 = "Số điên thoại không được bỏ trống!!!";
    }
    if (
        values.sdtTv5 &&
        !/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/.test(values.sdtTv5)
    ) {
        errors.sdtTv5 = 'Vui lòng nhập đúng định dạng SĐT'
    }
    if (!values.usernameTv5 || values.usernameTv5 === "") {
        errors.usernameTv5 = "Tên đăng nhập không được bỏ trống!!!";
    }
    if (
        values.usernameTv5 &&
        !/^[A-Za-z0-9_]{4,21}$/.test(values.usernameTv5)
    ) {
        errors.usernameTv5 = 'Tên đăng nhập phải có độ dài từ 4-21 ký tự'
    }

    return errors;
}