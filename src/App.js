import React, { Component } from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Navbar from './components/Navbar/Navbar';
import routes from './routes';
import './styles/style.scss';

class App extends Component {
  
  render(){
    return (
      <Router>
        <Navbar />
        <Switch>      
            {this.showMenu(routes)}
        </Switch>
      </Router>
    );
  }
  showMenu = () => {
    var results = null;
    if (routes.length > 0) {
      results = routes.map((route, index) => {
        return <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.main}
        />
      })
    }
    return <Switch>{results}</Switch>
  }
}

export default App;
