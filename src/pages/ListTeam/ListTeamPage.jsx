import React, { Component } from 'react';
import ListTeam from '../../components/Table/ListTeam';
import bnHead from '../../assets/imgs/list-team.png'
import axios from 'axios'

class ListTeamPage extends Component {

    state = {
        data:null,
    }

    componentDidMount(){
        axios.post("http://dotkich.goplay.vn/api/ajaxapi/OfflineTeamMember/GetAllTeamRegister")
        .then(res => {
            this.setState({
                data: res.data.data
            })
        })
    }

    render() {
        let {data} = this.state;
        return (
            <div>
                <div className="container">
                <div className="listTeam__content">
                    <div className="listTeam__content--head">
                        <img src={bnHead} alt="bn-head" />
                    </div>
                    {/*  */}
                    <div className="listItem__content--mid pb-4">
                        <div className="content__mid">
                            {data? data.map((item,key) => {
                                return <ListTeam key={key}
                                locationId={item.LocationId}
                                locationName={item.LocationName}
                                />
                            }) : "No data"}
                            
                        </div>
                        {/*  */}
                        
                    </div>

                </div>
            </div>
            </div>
        );
    }
}

export default ListTeamPage;
