import React, { Component } from 'react';
import RegisterForm from '../../components/Form/RegisterForm.jsx';
import swal from 'sweetalert';
import axios from 'axios'

class RegisterPage extends Component {
    state = {
        success: null,
        error: null,
        successed: null,

        cmndCap: null,
        cmndTv1: null,
        cmndTv2: null,
        cmndTv3: null,
        cmndTv4: null,
        cmndTv5: null,

    }

    handleChange = e => {
        let locId = e.target.value
        sessionStorage.setItem("locationIdChange", locId);
    }

    handleClick = e => {

        const name = e.target.name;

        if (e.target.files[0]) {
            let data = {
                file: e.target.files[0],
                name: e.target.files[0].name
            }
            const formData = new FormData();

            // Update the formData object 
            formData.append(
                data.name,
                data.file
            );
            axios.post("http://dotkich.goplay.vn/api/ajaxapi/OfflineTeamMember/uploadfilesendmail", formData)
                .then(res => {
                    let data = res.data
                    this.setState({
                        [name]: data
                    })
                })
        }
    }

    handleSubmit = async (data) => {
        let fdata = {
            LocationId: sessionStorage.getItem("locationIdChange") ?
                sessionStorage.getItem("locationIdChange")
                : sessionStorage.getItem("locationId"),
            TeamName: data.TeamName,
            HeadName: data.HeadName,
            lst_team_member:
                [
                    {
                        memberType: 0,
                        FullName: data.HeadName,
                        Email: data.emailCap,
                        Tel: data.sdtCap,
                        userName: data.usernameCap,
                        CMTLink: this.state.cmndCap
                    },
                    {
                        memberType: 1,
                        FullName: data.nameTv1,
                        Email: data.emailTv1,
                        Tel: data.sdtTv1,
                        userName: data.usernameTv1,
                        CMTLink: this.state.cmndTv1
                    },
                    {
                        memberType: 1,
                        FullName: data.nameTv2,
                        Email: data.emailTv2,
                        Tel: data.sdtTv2,
                        userName: data.usernameTv2,
                        CMTLink: this.state.cmndTv2
                    },
                    {
                        memberType: 1,
                        FullName: data.nameTv3,
                        Email: data.emailTv3,
                        Tel: data.sdtTv3,
                        userName: data.usernameTv3,
                        CMTLink: this.state.cmndTv3
                    },
                    {
                        memberType: 1,
                        FullName: data.nameTv4,
                        Email: data.emailTv4,
                        Tel: data.sdtTv4,
                        userName: data.usernameTv4,
                        CMTLink: this.state.cmndTv4
                    },
                    {
                        memberType: 2,
                        FullName: data.nameTv5,
                        Email: data.emailTv5,
                        Tel: data.sdtTv5,
                        userName: data.usernameTv5,
                        CMTLink: this.state.cmndTv5
                    }
                ]
        }
        console.log("fdata: ", fdata.LocationId);
        axios.post("http://dotkich.goplay.vn/api/ajaxapi/OfflineTeamMember/OfflineTeamRegister", fdata)
            .then(res => {
                this.setState({
                    success: res.data.message,
                    successed: res.data.Successed,
                    error: false
                })
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    success: false,
                    error: err.message
                })
            })
    }

    render() {
        var { success, error, successed } = this.state
        var locationId = sessionStorage.getItem("locationId")
        var locationName = sessionStorage.getItem("locationName")

        if (success || error) {

            if (error) {
                swal("Đăng ký thất bại !", "Vui lòng chọn tỉnh/ thành phố của bạn !!", "error");
                error = null;
                console.log(error)
            }
            else if (successed === false) {
                swal("Đăng ký thất bại !", success, "error");
                successed = null;
                console.log(successed)
            }
            else if (success) {
                swal("Đăng ký thành công !", success, "success");
                success = null;
                console.log(success);
            }
        }
        return (
            <div>
                <RegisterForm
                    onSubmit={(data) => this.handleSubmit(data)}
                    handleClick={this.handleClick}
                    locationId={locationId}
                    locationName={locationName}
                    cmndCap={this.state.cmndCap}
                    cmndTv1={this.state.cmndTv1}
                    cmndTv2={this.state.cmndTv2}
                    cmndTv3={this.state.cmndTv3}
                    cmndTv4={this.state.cmndTv4}
                    cmndTv5={this.state.cmndTv5}

                    handleChange={(e) => this.handleChange(e)}
                />
            </div>
        );
    }
}

export default RegisterPage;