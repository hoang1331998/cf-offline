export const location = [
    {
        value: '1',
        label: '1. Hà Nội',
    },
    {
        value: '3',
        label: '3. Vĩnh Phúc',
    },
    {
        value: '4',
        label: '4. Bắc Ninh',
    },
    {
        value: '5',
        label: '5. Hưng Yên',
    },
    {
        value: '6',
        label: '6. Hà Nam',
    },
    {
        value: '7',
        label: '7. Hải Dương',
    },
    {
        value: '8',
        label: '8. Hải Phòng',
    },
    {
        value: '9',
        label: '9. Thái Bình',
    },
    {
        value: '10',
        label: '10. Nam Định',
    },
    {
        value: '11',
        label: '11. Ninh Bình',
    },
    {
        value: '12',
        label: '12. Lai Châu',
    },
    {
        value: '13',
        label: '13. Lào Cai',
    },
    {
        value: '14',
        label: '14. Điện Biên',
    },
    {
        value: '15',
        label: '15. Yên Bái',
    },
    {
        value: '16',
        label: '16. Sơn La',
    },
    {
        value: '17',
        label: '17. Hòa Bình',
    },
    {
        value: '18',
        label: '18. Hà Giang',
    },
    {
        value: '19',
        label: '19. Cao Bằng',
    },
    {
        value: '20',
        label: '20. Tuyên Quang',
    },
    {
        value: '21',
        label: '21. Bắc Kạn',
    },
    {
        value: '22',
        label: '22. Lạng Sơn',
    },
    {
        value: '23',
        label: '23. Thái Nguyên',
    },
    {
        value: '24',
        label: '24. Bắc Giang',
    },
    {
        value: '25',
        label: '25. Quảng Ninh',
    },
    {
        value: '26',
        label: '26. Phú Thọ',
    },

    {
        value: '27',
        label: '27. Thanh Hóa',
    },
    {
        value: '28',
        label: '28. Nghệ An',
    },
    {
        value: '29',
        label: '29. Hà Tĩnh',
    },
    {
        value: '30',
        label: '30. Quảng Bình',
    },
    {
        value: '31',
        label: '31. Quảng Trị',
    },
    {
        value: '32',
        label: '32. Thừa Thiên Huế',
    },
    {
        value: '33',
        label: '33. Đà Nẵng',
    },
    {
        value: '34',
        label: '34. Quảng Nam',
    },

    {
        value: '35',
        label: '35. Quảng Ngãi',
    },
    {
        value: '36',
        label: '36. Bình Định',
    },
    {
        value: '37',
        label: '37. Phú Yên',
    },
    {
        value: '38',
        label: '38. Khánh Hòa',
    },
    {
        value: '39',
        label: '39. Ninh Thuận',
    },
    {
        value: '40',
        label: '40. Bình Thuận',
    },
    {
        value: '41',
        label: '41. Gia Lai',
    },
    {
        value: '42',
        label: '42. Kontum',
    },
    {
        value: '43',
        label: '43. Dak Lak',
    },
    {
        value: '44',
        label: '44. Lâm Đồng',
    },
    {
        value: '45',
        label: '45. Dak Nông',
    },
    {
        value: '46',
        label: '46. TP. Hồ Chí Minh',
    },

    {
        value: '47',
        label: '47. Bình Dương',
    },
    {
        value: '48',
        label: '48. Bình Phước',
    },
    {
        value: '49',
        label: '49. Tây Ninh',
    },
    {
        value: '50',
        label: '50. Đồng Nai',
    },
    {
        value: '51',
        label: '51. Bà Rịa Vũng Tàu',
    },
    {
        value: '52',
        label: '52. Long An',
    },
    {
        value: '53',
        label: '53. Đồng Tháp',
    },
    {
        value: '54',
        label: '54. Tiền Giang',
    },
    {
        value: '55',
        label: '55. Bến Tre',
    },
    {
        value: '56',
        label: '56. An Giang',
    },
    {
        value: '57',
        label: '57. Cần Thơ',
    },
    {
        value: '58',
        label: '58. Vĩnh Long',
    },
    {
        value: '59',
        label: '59. Trà Vinh',
    },

    {
        value: '60',
        label: '60. Kiên Giang',
    },
    {
        value: '61',
        label: '61. Hậu Giang',
    },
    {
        value: '62',
        label: '62. Sóc Trăng',
    },
    {
        value: '63',
        label: '63. Bạc Liêu',
    },
    {
        value: '64',
        label: '64. Cà Mau',
    }
];