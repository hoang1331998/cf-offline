import Home from "./pages/Home/Home";
import RegisterPage from "./pages/Register/RegisterPage";
import RulesPage from "./pages/Rules/RulesPage";
import React from 'react';
import ListTeamPage from "./pages/ListTeam/ListTeamPage";
import NotFound from './pages/NotFound/NotFound';

const routes = [
    {
        path: '/offline/',
        exact: true,
        main: () => <Home/>
    },
    {
        path: '/offline/dang-ky',
        exact: true,
        main: () => <RegisterPage/>
    },
    {
        path: '/offline/the-le',
        exact: true,
        main: () => <RulesPage/>
    },
    {
        path: '/offline/danh-sach-doi',
        exact: true,
        main: () => <ListTeamPage/>
    },
    {
        path: '',
        exact: true,
        main: () => <NotFound/>
    },
];

export default routes;