import React, { Component } from 'react';
import axios from 'axios'

class ListTeam extends Component {

    state = {
        data: null,
        sum: null
    }

    componentDidMount() {
        let locationId = this.props.locationId;
        let url = "http://dotkich.goplay.vn/api/ajaxapi/OfflineTeamMember/GetTeamLocation/?locationId=";
        axios.post(url + locationId)
            .then(res => {
                let data = res.data.data;
                this.setState({
                    data: data,
                    sum: data.length
                })
            })
    }
    render() {
        let { data, sum } = this.state;
        let count = 1;
        return (
            <div className="content__mid--inline">
                <div className="col-12" >
                    <div className="pt-4">
                        <h3 className="d--block">{this.props.locationName}</h3>
                        <div className="content__mid--sl">
                            <p className="text-white-50">Số đội tham dự </p>
                            <span className="d-inline-block badge bg-light">{sum}</span>
                        </div>
                    </div>
                    <table className="table tbCustom text-center">
                        <thead className="thead-dark">
                            <tr>
                                <th>STT</th>
                                <th>Tên đội</th>
                                <th>Đội Trưởng</th>
                            </tr>
                        </thead>
                        <tbody>
                            {data ? data.map((item, key) => {
                                return <tr key={key}>
                                    <td>{count++}</td>
                                    <td>{item.TeamName}</td>
                                    <td>{item.HeadName}</td>
                                </tr>
                            }) : <div className="text-center"><h5>Chưa có đội đăng ký</h5></div>}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListTeam;
