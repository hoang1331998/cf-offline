import React, { Component } from 'react';
import bnTop from '../../assets/imgs/title-thele.png'
import { Link } from 'react-router-dom'

class Rule extends Component {
    render() {
        return (
            <div className="rulePage">
                <div className="rulePage__bnTop">
                    <img src={bnTop} alt="bn-top" />
                </div>

                <div className="rulePage__content">
                    <div className="rulePage__content--inline">
                        <br />
                        <h5 className="content__text">
                        ĐỐI TƯỢNG THAM GIA
                        </h5>
                        <p className="content__text--inline">- Các người chơi, thành viên các người chơi đang hoạt động trong Đột Kích , Cấp độ từ 20 trở lên có thể lập Team thi đấu. </p>
                        <p className="content__text--inline">•	Tổng số người chơi Tham gia : 64 team ( tương ứng ~ 384 người ).</p>

                        <h5 className="content__text">A. GIAI ĐOẠN</h5>
                        <h6 className="content__text">1. Giai đoạn 1 : Mở thời gian Đăng kí Online – Trang Landing</h6>
                        <p className="content__text--inline">- Thời gian đăng kí : Từ ngày 19/10/2020 đến ngày 24/10/2020 ( 5 ngày kể từ ngày mở trang đăng kí )</p>
                        <p className="content__text--inline"> - Các team truy cập trang : http://dotkich.goplay.vn/offline để đăng kí thi đấu</p>
                        <h6 className="content__text">2. Giai đoạn 2: Bốc Thăm Chia Cặp Đấu</h6>
                        <p className="content__text--inline">- Thời gian: Từ ngày 25/10/2020 đến ngày 25/10/2020 ( 1 ngày kể từ ngày đăng kí hoàn tất )</p>
                        <p className="content__text--inline">- Livestream trực tuyến tại Fanpage Đột Kích Trong Tầm Ngắm và Đột Kích TV bốc thăm chia cặp đấu.</p>
                        <p className="content__text--inline">- Công bố danh sách cặp thi đấu – thời gian thi đấu trên Trang chủ , Fanpage Đột Kích Trong Tầm Ngắm và Đột Kích TV , các kênh truyền thông khác</p>
                        <h6 className="content__text">3. Giai Đoạn 3 : Thi Đấu Online</h6>
                        <p className="content__text--inline">- Thời gian: Từ ngày 31/10/2020 đến ngày 22/11/2020 ( 22 ngày kể từ bắt đầu thi đấu )</p>
                        <p className="content__text--inline">- Thời gian thi đấu sẽ được cố định trong khung giờ 16h và 20h các ngày thứ 7 và chủ nhật trong tuần.</p>
                        <p className="content__text--inline">- Các Team Leader sẽ lãnh đạo các thành viên thi đấu tham gia theo đúng khung giờ tổ chức do BTC công bố. </p>
                        <p className="content__text--inline">- Nếu trễ thời gian quá 15 phút mà vẫn chưa thể sẵn sàng tham chiến hoặc không có mặt, Team đó sẽ bị xử thua trắng và nhường quyền đi tiếp cho đội còn lại</p>
                        <p className="content__text--inline">- Thi đấu Online tìm ra 8 team mạnh nhất lọt vào vòng trong</p>
                        <p className="content__text--inline">- 2 GM và Caster làm trọng tài sử dụng SpectatorMode để theo dõi trận đấu</p>
                        <p className="content__text--inline">- Thể thức thi đấu Đặt Bom : 5 vs 5 BO3 loại trực tiếp ( 1 dự bị ) </p>
                        <p className="content__text--inline">- Round 10 : Đội nào đến 10 trước sẽ win.</p>
                        <p className="content__text--inline">- Map thi đấu : Sẽ có 5 Map : Trạm phát sóng, Thị trấn bỏ hoang , Dinh thự cấm , Tàu ngầm, Bến cảng được phép thi đấu. Khi bắt đầu thi đấu hai đội tuyển cấm chọn map thi đấu.</p>
                        <div className="pl-4">
                            <p className="content__text--inline">•	Mỗi đội cấm 1 map. Đội cấm trước sẽ phụ thuộc vào nhánh chẵn lẻ trên bảng bốc thăm. </p>
                        </div>
                        <h6 className="content__text">4. Giai Đoạn 4 : Thi Đấu Offline</h6>
                        <p className="content__text--inline">- Thời gian: Từ ngày<b> 28/11/2020</b> đến ngày<b> 12/12/2020</b> ( 7 ngày kể từ ngày Thi đấu online hoàn tất )</p>
                        <p className="content__text--inline">- Nhà phát hành sẽ lựa chọn địa điểm tổ chức Offline vòng chung kết tại một địa điểm.</p>
                        <p className="content__text--inline">- 8 đội mạnh nhất vòng bảng sẽ phải tập hợp và thi đấu tại địa chỉ Offline nhà phát hành công bố. Các chi phí di chuyển sẽ được nhà phát hành hỗ trợ.</p>
                        <p className="content__text--inline">- Trong trường hợp các đội Thắng được vào vòng trong không thể tham gia Offline ( với bất kỳ lý do gì ) thì đội đối đầu gần nhất sẽ được quyền thay thế để đi tiếp vào vòng trong.</p>

                        <h5 className="content__text">B. Luật Thi Đấu :</h5>
                        <p className="content__text--inline">- Riêng vòng Chung Kết sẽ chia bảng. Đấu vòng tròn tính điểm. Đội nhất bảng A sẽ gặp nhất bảng B trong trận Chung Kết.</p>
                        <p className="content__text--inline">- Nghiêm cấm các hình thức gian lận trong trận đấu, sử dụng hack/cheat/bug/mod hoặc các công cụ nhằm tạo lợi thế trong trận đấu.</p>
                        <p className="content__text--inline">- Phát hiện 1 thành viên trong team gian lân trong quá trình thi đấu:</p>
                        <div className="pl-4">
                            <p className="content__text--inline">•	Khóa vĩnh viễn tài khoản gian lận.</p>
                            <p className="content__text--inline">•	Xử thua toàn đội – cấm thi đấu các giải sau này của Đột Kích</p>
                        </div>
                        <p className="content__text--inline">-	Cấm sử dụng chức năng Bình xịt trong trận đấu, Nếu trọng tài phát hiện Team sử dụng bình xịt trong map Team đó sẽ bị xử thua 1 hiệp.</p>
                        <p className="content__text--inline">- Thể thức thi đấu Đặt Bom : 5 vs 5 BO3 loại trực tiếp ( 1 dự bị ) </p>
                        <p className="content__text--inline">- Round 10 : Đội nào đến 10 trước sẽ win.</p>
                        <div className="pl-4">
                            <p className="content__text--inline">•	Thành viên dự bị :1 thành viên (Mỗi thành viên có 1 dự bị sẽ được vào trận đấu khi có 1 thành viên bị Disconnect , Leader sẽ ra hiệu để trọng tài có thể tạm dừng trận đấu, nếu có thỏa thuận của cả 2 đại diện người chơi, trận đấu sẽ được đấu lại vào khung giờ khác vào ngày hôm sau)</p>
                        </div>
                        <p className="content__text--inline">- Trọng tài sẽ lập Room thi đấu vào khung giờ được công bố trước đó gồm mật khẩu phòng đấu trận đấu sẽ liên hệ với 2 đội trưởng.</p>
                        <p className="content__text--inline">- Quyết định của BTC sẽ là quyết định cuối cùng.</p>

                        <h5 className="content__text"><span className="wingding">&#216;</span>	TRANG BỊ CẤM SỬ DỤNG</h5>
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col" className="content__text" colSpan="4">TRANG BỊ CẤM SỬ DỤNG</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row" className="titleTb">STT</th>
                                    <th className="titleTb">Item</th>
                                    <th className="titleTb">Trang bị</th>
                                </tr>
                                <tr>
                                    <td className="text-center">1</td>
                                    <td className="text-center">Vũ khí chính</td>
                                    <td>Cấm các dòng súng SMG , MG , Shotgun. Rifleman cấm 9A91, M14EBR</td>
                                </tr>
                                <tr>
                                    <td className="text-center">2</td>
                                    <td className="text-center">Vũ khí phụ</td>
                                    <td>Cấm các vũ khí trên 7 viên đạn,Cấm các loại COP 357</td>
                                </tr>
                                <tr>
                                    <td className="text-center">3</td>
                                    <td className="text-center">Vũ khí cận chiến</td>
                                    <td>Không cấm</td>
                                </tr>
                                <tr>
                                    <td className="text-center">4</td>
                                    <td className="text-center">Lựu đạn</td>
                                    <td>Free</td>
                                </tr>
                                <tr>
                                    <td className="text-center">5</td>
                                    <td className="text-center">Nhân Vật</td>
                                    <td>Cấm các nhân vật có hiệu ứng giảm choáng , khói , đá</td>
                                </tr>
                            </tbody>
                        </table>
                        {/* <br />
                        <h6 className="content__text text-center">BẢNG ĐẤU Giai đoạn 2 TEAM khu vực</h6>
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col" className="content__text" colSpan="7">Bảng chia cặp thi đấu vòng loại TEAM </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row" rowSpan="2">STT</th>
                                    <th rowSpan="2">Mã trận</th>
                                    <th colSpan="3">Cặp đấu</th>
                                    <th rowSpan="2">Thời gian</th>
                                    <th rowSpan="2" >Kênh TD,GM tạo phòng có mật khẩu</th>
                                </tr>
                                <tr>
                                    <th >Name 1</th>
                                    <th >Name 2</th>
                                    <th >Điểm số</th>
                                </tr>

                                <tr>
                                    <td className="text-center">1</td>
                                    <td className="text-center">CFW1</td>
                                    <td></td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                                <tr>
                                    <td className="text-center">2</td>
                                    <td className="text-center">CFW2</td>
                                    <td></td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                                <tr>
                                    <td className="text-center">3</td>
                                    <td className="text-center">CFW3</td>
                                    <td></td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                                <tr>
                                    <td className="text-center">4</td>
                                    <td className="text-center">CFW4</td>
                                    <td></td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                                <tr>
                                    <td className="text-center">5</td>
                                    <td className="text-center">CFW5</td>
                                    <td></td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                                <tr>
                                    <td className="text-center">6</td>
                                    <td className="text-center">CFW6</td>
                                    <td></td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                                <tr>
                                    <td className="text-center">7</td>
                                    <td className="text-center">CFW7</td>
                                    <td></td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                                <tr>
                                    <td className="text-center">8</td>
                                    <td className="text-center">CFW8</td>
                                    <td></td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>

                            </tbody>
                        </table>

                        <br />

                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col" className="content__text" colSpan="6">Bảng chia cặp thi đấu vòng Tứ kết TEAM </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">STT</th>
                                    <th >Mã trận</th>
                                    <th colSpan="2">Cặp đấu</th>
                                    <th >Thời gian</th>
                                    <th >Kênh TD,GM tạo phòng có mật khẩu</th>
                                </tr>
                                <tr>
                                    <td className="text-center">1</td>
                                    <td className="text-center">TW9</td>
                                    <td className="text-center">Win TW1</td>
                                    <td className="text-center">Win TW8</td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                                <tr>
                                    <td className="text-center">2</td>
                                    <td className="text-center">TW10</td>
                                    <td className="text-center">Win TW2</td>
                                    <td className="text-center">Win TW7</td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                                <tr>
                                    <td className="text-center">3</td>
                                    <td className="text-center">TW11</td>
                                    <td className="text-center">Win TW3</td>
                                    <td className="text-center">Win TW6</td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                                <tr>
                                    <td className="text-center">4</td>
                                    <td className="text-center">TW12</td>
                                    <td className="text-center">Win TW4</td>
                                    <td className="text-center">Win TW5</td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>

                            </tbody>
                        </table>

                        <br />
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col" className="content__text" colSpan="6">Bảng chia cặp thi đấu vòng BÁN kết TEAM  </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">STT</th>
                                    <th >Mã trận</th>
                                    <th colSpan="2">Cặp đấu</th>
                                    <th >Thời gian</th>
                                    <th >Kênh TD,GM tạo phòng có mật khẩu</th>
                                </tr>
                                <tr>
                                    <td className="text-center">1</td>
                                    <td className="text-center">THẮNG 9 vs Thắng 11</td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                                <tr>
                                    <td className="text-center">2</td>
                                    <td className="text-center">THẮNG 10 vs Thắng 12</td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                            </tbody>
                        </table>


                        <br />
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col" className="content__text" colSpan="6">TRẬN TRANH GIẢI 3 4 TEAM</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">STT</th>
                                    <th >Mã trận</th>
                                    <th colSpan="2">Cặp đấu
                                    Thua 9vs11 gặp Thua 10vs12
                                    </th>
                                    <th >Thời gian</th>
                                    <th >Kênh TD,GM tạo phòng có mật khẩu</th>
                                </tr>
                                <tr>
                                    <td className="text-center">1</td>
                                    <td className="text-center">LBK1 vs LBK2</td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                            </tbody>
                        </table>

                        <br />
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col" className="content__text" colSpan="6">TRẬN CHUNG KẾT </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">STT</th>
                                    <th >Mã trận</th>
                                    <th colSpan="2">Cặp đấu</th>
                                    <th >Thời gian</th>
                                    <th >Kênh TD,GM tạo phòng có mật khẩu</th>
                                </tr>
                                <tr>
                                    <td className="text-center">1</td>
                                    <td className="text-center">TBK1 vs TBK2</td>
                                    <td className="text-center"></td>
                                    <td className="text-center"></td>
                                    <td className="text-center">20h00 …/…/2020</td>
                                    <td className="text-center"></td>
                                </tr>
                            </tbody>
                        </table>

                        <br />
                        <h5 className="content__text">V.	Hoạt động trong thời gian thi đấu giành cho cộng đồng:</h5>
                        <div className="pl-4">
                            <p className="content__text--inline">•	Stream trực tiếp các trận đấu Giai đoạn 2 : trên Fanpage<b> Đột Kích Trong Tầm Ngắm</b> và<b> Đột Kích TV</b></p>
                            <p className="content__text--inline">•	Phát code cho người theo dõi trực tiếp trên kênh Fanpage <b> Đột Kích Trong Tầm Ngắm</b> và<b> Đột Kích TV</b> – Số lượng 150 code cho 1 buổi. </p>
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="row">STT</th>
                                        <th >Nội dung </th>
                                        <th >Vật phẩm</th>
                                        <th >Ngày phát code</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="text-center" rowSpan="2">1</td>
                                        <td className="text-center" rowSpan="2">Code Xem Stream</td>
                                        <td className="text-center" >2 phiếu coupon</td>
                                        <td className="text-center" rowSpan="2" >Livestream</td>
                                    </tr>
                                    <tr>
                                        <td className="text-center">Bộ súng CFS 3 ngày</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <br />
                        <h5 className="content__text">VI.	GIẢI THƯỞNG:</h5>
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                    <th scope="col" className="content__text" colSpan="4">Giải thưởng  </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th className="text-center">STT</th>
                                    <th className="text-center">Thứ hạng</th>
                                    <th className="text-center">Số lượng</th>
                                    <th className="text-center">Phần thưởng (Tiền mặt)</th>
                                </tr>
                                <tr>
                                    <td className="text-center">1</td>
                                    <th className="text-center">Vô địch</th>
                                    <td className="text-center">1</td>
                                    <td className="text-center">150,000,000</td>
                                </tr>
                                <tr>
                                    <td className="text-center">2</td>
                                    <th className="text-center">Hạng nhì</th>
                                    <td className="text-center">1</td>
                                    <td className="text-center">70,000,000</td>
                                </tr>
                                <tr>
                                    <td className="text-center">3</td>
                                    <th className="text-center">Hạng ba</th>
                                    <td className="text-center">1</td>
                                    <td className="text-center">30,000,000</td>
                                </tr>
                                <tr>
                                    <td className="text-center">4</td>
                                    <th className="text-center">Hạng tư</th>
                                    <td className="text-center">1</td>
                                    <td className="text-center">15,000,000</td>
                                </tr>
                                <tr>
                                    <td className="text-center">5</td>
                                    <th className="text-center">Hạng khuyến khích</th>
                                    <td className="text-center">4</td>
                                    <td className="text-center">20,000,000</td>
                                </tr>
                                <tr>
                                    <td className="text-center" colSpan="3">Tổng Chi Phí</td>
                                    <td className="text-center">285,000,000</td>
                                </tr>
                            </tbody>
                        </table> */}

                        {/*  */}
                        <div className="content__btn">
                            <Link className="btnSubmit" to="/offline/dang-ky">&nbsp;</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Rule;