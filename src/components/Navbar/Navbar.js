import React, { useState } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    Nav,
    NavItem,
} from 'reactstrap';
import { Link, NavLink } from 'react-router-dom'
import logo from '../../assets/imgs/logo.png'

const Example = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
            <Navbar className="navBar" color="dark" light expand="md">
                <Link to='/offline/'  ><img src={logo} alt="logo" refresh="true" /></Link>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav
                        className="mr-auto navBar__menu" navbar>
                        <NavItem className=" navLink">
                            <NavLink exact
                                activeClassName="active" 
                                className="navItem " to="/offline/" refresh="true">Trang Chủ</NavLink>
                        </NavItem>
                        <NavItem className="navLink">
                            <NavLink exact
                                activeClassName="active" 
                                className="navItem" to="/offline/the-le">thể lệ</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        </div>
    );
}

export default Example;