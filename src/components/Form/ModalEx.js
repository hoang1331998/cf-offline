import React, { useState } from 'react';
import { Modal, ModalBody } from 'reactstrap';
import MapVN from '../../assets/imgs/map.png'

const ModalExample = (props) => {
  const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <div className="btnGuide" onClick={toggle}>{buttonLabel}</div>
      <Modal isOpen={modal} toggle={toggle} className={className}> 
        <ModalBody>
         <img className="imgMap" src={MapVN} alt="map-vn"/>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default ModalExample;