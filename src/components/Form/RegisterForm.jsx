import React, { Component } from 'react';
import bnHead from '../../assets/imgs/title-resgister.png'
import { FormGroup, Label, Col, Row } from 'reactstrap'
import { renderField} from '../Field/RenderField'
import { reduxForm, Field } from "redux-form";
import { RegisterValidations } from '../../validations/RegisterForm'
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import {location} from '../../assets/data/Data'


class RegisterForm extends Component {

    render() {

        let { locationName } = this.props;

        return (
            <div className="container">
                <div className="content">
                    {/*  */}
                    <div className="content__head">
                        <img src={bnHead} alt="" />
                    </div>
                    {/*  */}
                    <div className="content__form">
                        <div className="form">
                            <form
                                onSubmit={this.props.handleSubmit}>
                                <div className="col-md-12">
                                <label htmlFor="LocationId">Bạn vui lòng chọn tỉnh thành đăng ký</label><br/>
                                    <select
                                        name="LocationId"
                                        onChange={this.props.handleChange}
                                    >
                                        <option>{locationName ? locationName : "Bạn vui lòng chọn tỉnh thành đăng ký"}</option>
                                        {location.length > 0 ? location.map((item,key) =>
                                        <option key={key} value={item.value}>{item.label}</option>
                                    ) : ''};
                                    </select>
                                </div>
                                {/*  */}
                                <div className="nameGroup col-md-6">
                                    <label htmlFor="TeamName">Tên đội</label>
                                    <Field
                                        className="form-control"
                                        component={renderField}
                                        name="TeamName"
                                        type="text"
                                        placeholder="Tên đội" />
                                </div>
                                {/*  */}

                                <FormGroup className="rowContent">
                                    <h3>Đội trưởng</h3>
                                    <Row>
                                        <Col md="3">
                                            <Label htmlFor="name" className="col-form-label ">
                                                Họ và tên
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="HeadName"
                                                lanel="Name "
                                                placeholder="Họ và tên"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="3">
                                            <Label htmlFor="email" className="col-form-label">
                                                Email
                                             </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="emailCap"
                                                placeholder="Email"
                                                lanel="Name "
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="sdt" className="col-form-label">
                                                Số điện thoại
                                             </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="sdtCap"
                                                placeholder="Số điện thoại"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="username" className="col-form-label">
                                                Tên đăng nhập
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="usernameCap"
                                                placeholder="Tên đăng nhập"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="cmnd" className="col-form-label">
                                                CMND
                                            </Label>
                                            <div className="cmnd">
                                                <input
                                                    name="cmndCap"
                                                    type="file"
                                                    className="fileUpload"
                                                    title=" "
                                                    value=""
                                                    required
                                                    onChange={this.props.handleClick}
                                                />
                                                {this.props.cmndCap ? <CheckCircleIcon className="doneIcon" /> : ""}

                                            </div>
                                        </Col>
                                    </Row>
                                </FormGroup>
                                {/*  */}
                                <FormGroup className="rowContent" >
                                    <h3>Thành viên 1</h3>
                                    <Row>
                                        <Col md="3">
                                            <Label htmlFor="name" className="col-form-label ">
                                                Họ và tên
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="nameTv1"
                                                lanel="Name "
                                                placeholder="Họ và tên"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="3">
                                            <Label htmlFor="email" className="col-form-label">
                                                Email
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="emailTv1"
                                                placeholder="Email"
                                                lanel="Name "
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="sdt" className="col-form-label">
                                                Số điện thoại
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="sdtTv1"
                                                placeholder="Số điện thoại"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="username" className="col-form-label">
                                                Tên đăng nhập
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="usernameTv1"
                                                placeholder="Tên đăng nhập"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="cmnd" className="col-form-label">
                                                CMND
                                            </Label>
                                            <div className="cmnd">
                                                <input
                                                    name="cmndTv1"
                                                    type="file"
                                                    className="fileUpload"
                                                    required
                                                    title=" "
                                                    value=""
                                                    onChange={this.props.handleClick}
                                                />
                                                {this.props.cmndTv1 ? <CheckCircleIcon className="doneIcon" /> : ""}
                                            </div>
                                        </Col>
                                    </Row>
                                </FormGroup>
                                {/*  */}
                                <FormGroup className="rowContent" >
                                    <h3>Thành viên 2</h3>
                                    <Row>
                                        <Col md="3">
                                            <Label htmlFor="name" className="col-form-label ">
                                                Họ và tên
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="nameTv2"
                                                lanel="Name "
                                                placeholder="Họ và tên"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="3">
                                            <Label htmlFor="email" className="col-form-label">
                                                Email
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="emailTv2"
                                                placeholder="Email"
                                                lanel="Name "
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="sdt" className="col-form-label">
                                                Số điện thoại
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="sdtTv2"
                                                placeholder="Số điện thoại"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="username" className="col-form-label">
                                                Tên đăng nhập
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="usernameTv2"
                                                placeholder="Tên đăng nhập"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="cmnd" className="col-form-label">
                                                CMND
                                            </Label>
                                            <div className="cmnd">
                                                <input
                                                    name="cmndTv2"
                                                    type="file"
                                                    className="fileUpload"
                                                    required
                                                    title=" "
                                                    value=""
                                                    onChange={this.props.handleClick}
                                                />
                                                {this.props.cmndTv2 ? <CheckCircleIcon className="doneIcon" /> : ""}
                                            </div>
                                        </Col>
                                    </Row>
                                </FormGroup>
                                {/*  */}
                                <FormGroup className="rowContent" >
                                    <h3>Thành viên 3</h3>
                                    <Row>
                                        <Col md="3">
                                            <Label htmlFor="name" className="col-form-label ">
                                                Họ và tên
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="nameTv3"
                                                lanel="Name "
                                                placeholder="Họ và tên"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="3">
                                            <Label htmlFor="email" className="col-form-label">
                                                Email
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="emailTv3"
                                                placeholder="Email"
                                                lanel="Name "
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="sdt" className="col-form-label">
                                                Số điện thoại
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="sdtTv3"
                                                placeholder="Số điện thoại"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="username" className="col-form-label">
                                                Tên đăng nhập
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="usernameTv3"
                                                placeholder="Tên đăng nhập"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="cmnd" className="col-form-label">
                                                CMND
                                            </Label>
                                            <div className="cmnd">
                                                <input
                                                    name="cmndTv3"
                                                    type="file"
                                                    className="fileUpload"
                                                    required
                                                    title=" "
                                                    value=""
                                                    onChange={this.props.handleClick}
                                                />
                                                {this.props.cmndTv3 ? <CheckCircleIcon className="doneIcon" /> : ""}
                                            </div>
                                        </Col>
                                    </Row>
                                </FormGroup>
                                {/*  */}
                                <FormGroup className="rowContent" >
                                    <h3>thành viên 4</h3>
                                    <Row>
                                        <Col md="3">
                                            <Label htmlFor="name" className="col-form-label ">
                                                Họ và tên
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="nameTv4"
                                                lanel="Name "
                                                placeholder="Họ và tên"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="3">
                                            <Label htmlFor="email" className="col-form-label">
                                                Email
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="emailTv4"
                                                placeholder="Email"
                                                lanel="Name "
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="sdt" className="col-form-label">
                                                Số điện thoại
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="sdtTv4"
                                                placeholder="Số điện thoại"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="username" className="col-form-label">
                                                Tên đăng nhập
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="usernameTv4"
                                                placeholder="Tên đăng nhập"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="cmnd" className="col-form-label">
                                                CMND
                                            </Label>
                                            <div className="cmnd">
                                                <input
                                                    name="cmndTv4"
                                                    type="file"
                                                    className="fileUpload"
                                                    required
                                                    title=" "
                                                    value=""
                                                    onChange={this.props.handleClick}
                                                />
                                                {this.props.cmndTv4 ? <CheckCircleIcon className="doneIcon" /> : ""}
                                            </div>
                                        </Col>
                                    </Row>
                                </FormGroup>
                                {/*  */}
                                <FormGroup className="rowContent" >
                                    <h3>dự bị</h3>
                                    <Row>
                                        <Col md="3">
                                            <Label htmlFor="name" className="col-form-label ">
                                                Họ và tên
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="nameTv5"
                                                lanel="Name "
                                                placeholder="Họ và tên"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="3">
                                            <Label htmlFor="email" className="col-form-label">
                                                Email
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="emailTv5"
                                                placeholder="Email"
                                                lanel="Name "
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="sdt" className="col-form-label">
                                                Số điện thoại
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="sdtTv5"
                                                placeholder="Số điện thoại"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="username" className="col-form-label">
                                                Tên đăng nhập
                                            </Label>
                                            <Field
                                                id="outlined-textarea"
                                                component={renderField}
                                                name="usernameTv5"
                                                placeholder="Tên đăng nhập"
                                                type="text"
                                            />
                                        </Col>
                                        <Col md="2">
                                            <Label htmlFor="cmnd" className="col-form-label">
                                                CMND
                                            </Label>
                                            <div className="cmnd">
                                                <input
                                                    name="cmndTv5"
                                                    type="file"
                                                    className="fileUpload"
                                                    required
                                                    title=" "
                                                    value=""
                                                    onChange={this.props.handleClick}
                                                />
                                                {this.props.cmndTv5 ? <CheckCircleIcon className="doneIcon" /> : ""}
                                            </div>
                                        </Col>
                                    </Row>
                                </FormGroup>
                                <FormGroup className="row justify-content-center">
                                    <div className="bx-btn w-50 pt-4 pb-4">
                                        <button className="btnRegister">&nbsp;</button>
                                    </div>
                                </FormGroup>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
RegisterForm = reduxForm({
    form: "formRegisterForm",
    validate: RegisterValidations,
    enableReinitialize: true,
})(RegisterForm);
export default RegisterForm;
